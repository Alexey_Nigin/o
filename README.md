# O

My sci-fi choose-your-own-adventure story from long long ago!

## Credits

* Story/code by Alexey Nigin
  ([CC BY-NC](https://creativecommons.org/licenses/by-nc/4.0/))
* Background image by
  <a href="https://opengameart.org/content/space-background-01">[Satur9](https://opengameart.org/content/space-background-01)
  ([public domain](https://creativecommons.org/publicdomain/zero/1.0/))
